package com.mimose.component.redisson.common.config;

import com.mimose.component.redisson.cache.util.CacheUtil;
import com.mimose.component.redisson.cache.api.client.RedissonCacher;
import com.mimose.component.redisson.topic.util.TopicCreater;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;

import javax.annotation.PostConstruct;

/**
 * @Description 基本工具类初始化（redisson）
 * @Author ccy
 * @Date 2020/2/18
 */
@Slf4j
@Configuration
@ConditionalOnBean(value = RedissonClientConfiguration.class)
@AutoConfigureOrder(Ordered.LOWEST_PRECEDENCE + 105)
public class RedissonUtilConfiguration {

    @Autowired
    private RedissonClient redissonClient;

    @PostConstruct
    public void afterClient(){
        configRMapCache();
        configTopic();
    }

    /**
     * redisson基本存取配置类
     */
    private void configRMapCache() {
        RedissonCacher cacher = new RedissonCacher(redissonClient);
        CacheUtil.setCacher(cacher);
        log.info("start init redisson cacher success");
    }

    /**
     * redisson订阅工具类
     */
    private void configTopic() {
//        log.info("start init topic creater ...");
        TopicCreater.setRedissonClient(redissonClient);
        log.info("start init topic creater success");
    }
}
